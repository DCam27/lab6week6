# CHESS

# Pruebas

## Prueba 1

### Entrada
![Ejemplo 1](resources/img_12.png)

### Resultado esperado
![Ejemplo 1](resources/img_13.png)

### Resultado obtenido
![Ejemplo 1](resources/img_14.png)

## Prueba 2

### Entrada
![Ejemplo 1](resources/img_15.png)

### Resultado esperado
En esta prueba el resultado esperado dado en el laboratorio no usa la entrada especificada mediante CLI asi que tenemos diferencias en la salida pero cumple con el ordemnamiento siendo el algoritmo de inserction por defecto para mi programa y el color "Blancas" para el color por defecto si no se ingresan esos parametros
![Ejemplo 1](resources/img_16.png)

### Resultado obtenido
![Ejemplo 1](resources/img_17.png)

## Prueba 3

### Entrada
![Ejemplo 1](resources/img_19.png)

### Resultado esperado
En esta prueba el resultado esperado dado en el laboratorio no usa la entrada especificada mediante CLI asi que tenemos diferencias en la salida pero cumple con el ordemnamiento siendo el algoritmo de inserction por defecto para mi programa y el color "Blancas" para el color por defecto si no se ingresan esos parametros
![Ejemplo 1](resources/img_18.png)

### Resultado obtenido
![Ejemplo 1](resources/img_20.png)



# Diagrama de clases
![Ejemplo 1](resources/img_9.png)
![Ejemplo 1](resources/img_10.png)
## Codigo mermaid
```
classDiagram
  class ParametersValidator {
    -parameters: HashMap<String, String>
    +ParametersValidator(arguments: String[])
    +getParameters(): HashMap<String, String>
    +validateVariableValues(): boolean
    +validateVariables(arguments: String[]): boolean
    +validateColour(colour: String): boolean
    +validateType(valuesType: String): boolean
    +validateAlgorithmSelection(algorithmName: String): boolean
    +containsOnlyLetters(input: String): boolean
    +containsOnlyNumbers(input: String): boolean
  }

  class ParametersManager {
    -parameters: HashMap<String, String>
    -validator: ParametersValidator
    +ParametersManager(arguments: String[])
    +getAlgorithm(): String
    +getValues(): String
    +getValuesType(): String
    +getColour(): String
    -getFormatedValuesTypeString(): String
    -getFormatedColourString(): String
    -getFormatedAlgorithmString(): String
    +printParameters(): void
  }
   class interface Sorter~T~{
    +sort(list: ArrayList~T~): ArrayList~T~
  }

  class InsertionSort~T extends Comparable~ {
    +sort(list: ArrayList~T~): ArrayList~T~
  }

  class QuickSort~T extends Comparable~ {
    +sort(list: ArrayList~T~): ArrayList~T~
  }

  class BubbleSort~T extends Comparable~ {
    +sort(list: ArrayList~T~): ArrayList~T~
  }

  ParametersValidator --|> ParametersManager
  Sorter <|.. InsertionSort
  Sorter <|.. QuickSort
  Sorter <|.. BubbleSort

 
```

# Clases
## Parameters validator

La clase está diseñada para encapsular la lógica de validación de parámetros de línea de comandos y
proporcionar métodos para obtener y verificar estos parámetros. Si hay problemas en la validación, se muestran mensajes
de error en la consola.

### Atributo parameters:

Es un mapa (HashMap) que almacena los parámetros y sus valores como pares clave-valor.

### Constructor ParametersValidator(String[] arguments):

Toma un array de String como argumento, que supuestamente contiene los parámetros de la línea de comandos.
Llama a los métodos validateVariables(arguments) y validateVariableValues() para realizar la validación de los
parámetros.

### Método validateVariables(String[] arguments):

Inicializa el mapa parameters.
Itera sobre el array de arguments y parsea los valores para llenar el mapa.
Muestra mensajes de error si se detectan problemas en los parámetros (longitud insuficiente, variables no válidas).
Genera valores por defecto para la variable v si no se proporciona.
Verifica si ciertos parámetros esenciales (a, t, o) están presentes.

#### Método validateVariableValues():

Realiza validaciones específicas para ciertos parámetros (a, t, o).
Llama a métodos específicos de validación (validateAlgorithmSelection, validateType, validateColour).
Devuelve true si todas las validaciones son exitosas.

#### Métodos de validación específicos:

validateAlgorithmSelection(String algorithmName): Verifica si el algoritmo seleccionado es "i" o "q".

validateType(String valuesType): Verifica si el tipo de valores (v) es "c" (solo letras) o "n" (solo números).

validateColour(String colour): Verifica si el color especificado es "b" (negro) o "w" (blanco).

#### Métodos auxiliares:

containsOnlyLetters(String input): Verifica si la cadena solo contiene letras.
containsOnlyNumbers(String input): Verifica si la cadena solo contiene números.

## Parameters Manager

La clase ParametersManager actúa como un intermediario entre los parámetros de línea de comandos y el resto
del programa. Utiliza la clase ParametersValidator para validar los parámetros y luego proporciona métodos para acceder
a estos parámetros validados y formatear su presentación para su visualización en la consola.

### Atributos:

parameters: Un mapa (HashMap) que almacena los parámetros y sus valores, obtenidos del validador.
validator: Una instancia de la clase ParametersValidator utilizada para la validación de parámetros.

### Constructor ParametersManager(String[] arguments):

Crea una instancia de ParametersValidator pasándole los argumentos de línea de comandos.
Captura excepciones en caso de error durante la validación.
Obtiene el mapa de parámetros validados y lo asigna al atributo parameters.

### Métodos getAlgorithm(), getValues(), getValuesType(), getColour():

Métodos simples que devuelven los valores específicos de los parámetros (a, v, t, o) del mapa parameters.

### Métodos privados getFormatedValuesTypeString(), getFormatedColourString(), getFormatedAlgorithmString():

Estos métodos formatean las representaciones de cadena de ciertos valores específicos del mapa parameters (t, o, a) para
imprimir información más legible.
Capturan excepciones en caso de que los valores sean nulos o no válidos.

### Método printParameters():

Imprime en la consola información formateada sobre los parámetros, utilizando los métodos privados de formato.
Muestra el tipo de algoritmo, tipo de valores, color y los valores específicos (v).

## Interfaz sorter

La interfaz Sorter<T> define las firmas para las clases que implementarán algoritmos de ordenamiento. Las
implementaciones concretas de esta interfaz se encargarán de proporcionar la lógica específica para ordenar elementos de
la lista.

### Genérico:

La interfaz es genérica y utiliza el parámetro de tipo <T>. Esto significa que puede ser implementada para ordenar
listas de cualquier tipo de objeto.

### Método sort(ArrayList<T> list):

La interfaz declara un único método llamado sort que toma como argumento un ArrayList de elementos de tipo T.
El método se encarga de ordenar la lista de elementos y devuelve una nueva lista ordenada. La lista original no debería
ser modificada.

### Propósito:

La interfaz Sorter proporciona una estructura común para diferentes algoritmos de ordenamiento.
Al tener esta interfaz, se pueden crear diferentes clases que implementen algoritmos de ordenamiento específicos (por
ejemplo, inserción, quicksort) y se pueden intercambiar fácilmente dentro de un programa sin cambiar el código cliente.
Flexibilidad:

La flexibilidad del tipo genérico permite que esta interfaz sea utilizada para ordenar listas de cualquier tipo de
objeto, siempre y cuando esos objetos sean comparables entre sí (implementen la interfaz Comparable o se proporcionen
comparadores).

## Clase Insertion Sort

La clase InsertionSort<T> proporciona una implementación específica del algoritmo de ordenamiento por
inserción para listas de elementos comparables. Puede ser utilizada como una estrategia de ordenamiento dentro de un
contexto más amplio.

### Extiende la interfaz Sorter<T>:

La clase InsertionSort<T> implementa la interfaz genérica Sorter<T>, lo que significa que debe proporcionar una
implementación para el método sort(ArrayList<T> list) definido en la interfaz.

### Genérico con restricción Comparable:

La clase es genérica con el tipo <T>, pero con una restricción que indica que T debe ser comparable (T extends
Comparable<T>). Esto asegura que los elementos a ordenar sean comparables entre sí.

### Método sort(ArrayList<T> list):

Este método implementa el algoritmo de ordenamiento por inserción.
Se crea una nueva lista (sortedList) copiando los elementos de la lista original para no modificar la lista original
durante el proceso de ordenamiento.
Luego, se itera a través de la lista desde el segundo elemento hasta el final.
En cada iteración, se toma un elemento (key) y se compara con los elementos anteriores. Se desplazan los elementos
mayores hacia la derecha hasta encontrar la posición correcta para key.
El resultado es una lista ordenada que se devuelve al final del método.

### Funcionamiento del algoritmo:

El algoritmo de ordenamiento por inserción compara cada elemento con los elementos anteriores y lo inserta en la
posición correcta, construyendo así una parte ordenada de la lista en cada iteración.
Ventajas y Desventajas:

### Ventajas:

Es simple y eficiente para listas pequeñas o casi ordenadas.

### Desventajas:

No es eficiente para listas grandes y su complejidad es O(n^2) en el peor caso.

## Clase Quick Sort

La clase QuickSort<T> proporciona una implementación específica del algoritmo de ordenamiento quicksort para
listas de elementos comparables. Puede ser utilizada como una estrategia de ordenamiento en programas que requieren
ordenamiento eficiente de listas.

### Extiende la interfaz Sorter<T>:

Al igual que la clase InsertionSort<T>, la clase QuickSort<T> implementa la interfaz genérica Sorter<T>, lo que
significa que proporciona una implementación para el método sort(ArrayList<T> list).

### Genérico con restricción Comparable:

La clase es genérica con el tipo <T>, pero con la restricción de que T debe ser comparable (T extends Comparable<T>),
asegurando que los elementos a ordenar sean comparables entre sí.

### Método sort(ArrayList<T> list):

Este método implementa el algoritmo de ordenamiento quicksort.

Si la lista tiene un tamaño igual o inferior a 1, se considera que está ordenada y se devuelve tal cual.

Se elige un elemento central como el "pivote" (pivotValue). En este caso, se selecciona el elemento en la posición
central de la lista.

Se crean tres listas (less, equal, y greater) para almacenar los elementos que son menores, iguales y mayores que el
pivote, respectivamente.

Se itera a través de la lista original, comparando cada elemento con el pivote y colocándolo en la lista
correspondiente.

Se llama recursivamente al método sort para las listas less y greater.

Se concatenan las listas ordenadas (less, equal, greater) para obtener la lista final ordenada.

### Funcionamiento del algoritmo:

Quicksort es un algoritmo de ordenamiento eficiente y de tipo dividir y conquistar. Divide la lista en sub-listas más
pequeñas, las ordena de forma recursiva y luego combina las sublistas ordenadas.

### Ventajas:

Eficiente en listas grandes y su complejidad promedio es O(n log n).

### Desventajas:

Puede ser menos eficiente en listas pequeñas y tiene un peor caso O(n^2) si no se elige un pivote adecuado.

## Clase Bubble Sort

La clase BubbleSort<T> proporciona una implementación específica del algoritmo de ordenamiento bubble sort
para listas de elementos comparables. Aunque es fácil de entender, bubble sort no suele ser la opción más eficiente para
listas grandes debido a su complejidad cuadrática.

### Extiende la interfaz Sorter<T>:

Al igual que las clases anteriores, la clase BubbleSort<T> implementa la interfaz genérica Sorter<T>, proporcionando así
una implementación para el método sort(ArrayList<T> list).

### Genérico con restricción Comparable:

La clase es genérica con el tipo <T>, pero con la restricción de que T debe ser comparable (T extends Comparable<T>),
asegurando que los elementos a ordenar sean comparables entre sí.

### Método sort(ArrayList<T> list):

Este método implementa el algoritmo de ordenamiento bubble sort.
Se utiliza un bucle anidado para iterar sobre la lista y comparar elementos adyacentes.
En cada iteración externa (i), se compara cada par de elementos adyacentes en la iteración interna (j).
Si el elemento en la posición j es mayor que el elemento en la posición j + 1, se intercambian.
Este proceso se repite hasta que toda la lista esté ordenada.

### Funcionamiento del algoritmo:

Bubble sort funciona comparando y intercambiando elementos adyacentes hasta que toda la lista esté ordenada.
En cada iteración, el elemento más grande "sube" a su posición final, similar a cómo suben las burbujas en un líquido.
Ventajas y Desventajas:

### Ventajas:

Es simple y fácil de entender.

### Desventajas:

Ineficiente para listas grandes, especialmente en comparación con otros algoritmos más eficientes como
quicksort o mergesort. Su complejidad es O(n^2) en el peor caso.




