package Activity2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class SistemaVotacionTest {
    @Test
    public void ejm1EscrutinioTest() {
        String[] candidatos = {
                "Candidato A 1",
                "Candidato A 1",
                "Candidato B 1",
                "Candidato C 1",
                "Candidato A 1",
                "Candidato C 1",
                "Candidato B 1",
                "Candidato B 1",
                "Candidato A 1",
                "Candidato A 1"};

        SistemaVotacion sistemaVotacion = new SistemaVotacion();
        sistemaVotacion.agregarVotos(candidatos);
        assertEquals("Escrutinio:\n" +
                "Candidato A , 5\n" +
                "Candidato B , 3\n" +
                "Candidato C , 2\n", sistemaVotacion.calcularEscrutinio());


    }
}

