package Activity2;

import java.util.LinkedHashMap;
import java.util.Map;

public class SistemaVotacion {
    private Map<String, Integer> escrutinio;

    public SistemaVotacion() {
        this.escrutinio = new LinkedHashMap<>();
    }

    public String calcularEscrutinio() {
        StringBuilder resultado = new StringBuilder("Escrutinio:\n");
        for (Map.Entry<String, Integer> entry : escrutinio.entrySet()) {
            resultado.append(entry.getKey()).append(", ").append(entry.getValue()).append("\n");
        }
        return resultado.toString();
    }

    public void agregarVotos(String[] candidatos) {
        for (String candidato : candidatos) {
            agregarVoto(candidato);
        }
    }

    public void agregarVoto(String candidato) {
        candidato = candidato.replace("1", "");
        if (escrutinio.containsKey(candidato)) {
            escrutinio.put(candidato, escrutinio.get(candidato) + 1);
        } else {
            escrutinio.put(candidato, 1);
        }
    }
}