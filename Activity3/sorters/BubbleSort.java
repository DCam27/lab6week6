package Activity3.sorters;

import java.util.ArrayList;

public class BubbleSort<T extends Comparable<T>> implements Sorter<T> {


  @Override
  public ArrayList<T> sort(ArrayList<T> list) {
    int n = list.size();
    for (int i = 0; i < n - 1; i++) {
      for (int j = 0; j < n - i - 1; j++) {
        if (list.get(j).compareTo(list.get(j + 1)) > 0) {
          T temp = list.get(j);
          list.set(j, list.get(j + 1));
          list.set(j + 1, temp);
        }
      }
    }
    return list;
  }


}