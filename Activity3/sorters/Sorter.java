package Activity3.sorters;
import java.util.ArrayList;
import java.util.List;

public interface Sorter<T> {

  List<T> sort(ArrayList<T> list);

}

