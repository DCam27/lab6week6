package Activity3.sorters;


import java.util.ArrayList;

public class QuickSort<T extends Comparable<T>> implements Sorter<T> {

    @Override
    public ArrayList<T> sort(ArrayList<T> list) {
        if (list.size() <= 1) {
            return list;
        }

        int pivotIndex = list.size() / 2;
        T pivotValue = list.get(pivotIndex);

        ArrayList<T> less = new ArrayList<>();
        ArrayList<T> equal = new ArrayList<>();
        ArrayList<T> greater = new ArrayList<>();

        for (T item : list) {
            int cmp = item.compareTo(pivotValue);
            if (cmp < 0) {
                less.add(item);
            } else if (cmp > 0) {
                greater.add(item);
            } else {
                equal.add(item);
            }
        }

        ArrayList<T> sortedList = new ArrayList<>();
        sortedList.addAll(sort(less));
        sortedList.addAll(equal);
        sortedList.addAll(sort(greater));

        return sortedList;
    }

}
