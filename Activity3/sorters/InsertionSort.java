package Activity3.sorters;

import java.util.ArrayList;

public class InsertionSort<T extends Comparable<T>> implements Sorter<T> {

  @Override
  public ArrayList<T> sort(ArrayList<T> list) {
    ArrayList<T> sortedList = new ArrayList<>(list);

    int n = sortedList.size();
    for (int i = 1; i < n; i++) {
      T key = sortedList.get(i);
      int j = i - 1;

      while (j >= 0 && key.compareTo(sortedList.get(j)) < 0) {
        sortedList.set(j + 1, sortedList.get(j));
        j--;
      }

      sortedList.set(j + 1, key);
    }

    return sortedList;
  }
}
