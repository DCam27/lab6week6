package Activity3.parameters;

import java.util.HashMap;
import java.util.Set;

public class ParametersManager {

    private HashMap<String, String> parameters;
    private ParametersValidator validator;

    public ParametersManager(String[] arguments) {
        try {
            validator = new ParametersValidator(arguments);
            this.parameters = validator.getParameters();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public String getAlgorithm() {
        return parameters.get("a");
    }

    public String getValues() {
        return parameters.get("v");
    }

    public String getValuesType() {
        return parameters.get("t");
    }

    public String getColour() {
        return parameters.get("o");
    }

    private String getFormatedValuesTypeString() {
        try {
            if (parameters.get("t").equals("n"))
                return "Numerico";
            if (parameters.get("t").equals("c"))
                return "Caracter";
            return "Invalido";
        } catch (NullPointerException e) {
            System.out.println("Error en la entrada de tipo de valores");
            return "Invalido";
        }
    }

    private String getFormatedColourString() {
        try {
            if (parameters.get("o").equals("b"))
                return "Negras";
            if (parameters.get("o").equals("w"))
                return "Blancas";
            return "Invalido";
        } catch (NullPointerException e) {
            System.out.println("Error en la entrada de color");
            return "Invalido";
        }
    }

    private String getFormatedAlgorithmString() {
        try {
            if (parameters.get("a").equals("i"))
                return "Insertion sort";
            if (parameters.get("a").equals("q"))
                return "Quick sort";
            if (parameters.get("a").equals("b"))
                return "Bubble sort";
            return "Invalido";
        } catch (NullPointerException e) {
            System.out.println("Error en la entrada de tipo de algoritmo");
            return "Invalido";
        }

    }

    public void printParameters() {
        System.out.printf("Ordenamiento: [%s]\n"
                + "Tipo: [%s]\n"
                + "Color: [%s]\n"
                + "Valores: \n[%s]\n", getFormatedAlgorithmString(), getFormatedValuesTypeString(), getFormatedColourString(), parameters.get("v"));

    }
}
