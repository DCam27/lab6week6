package Activity3.parameters;
import Activity3.sorters.*;

import java.util.ArrayList;
import java.util.Arrays;


public class Main {
    public static void main(String[] args) {
        try {
            ParametersManager parametersManager = new ParametersManager(args);
            parametersManager.printParameters();
            Sorter sorter;
            if (parametersManager.getValues().length() == 0)
                System.out.println("Valores invalidos");
            else {
                if (parametersManager.getAlgorithm().equals("q")) {
                    sorter = new QuickSort();
                    System.out.printf("Ordenamiento: \n%s", sorter.sort(new ArrayList<>(Arrays.asList(parametersManager.getValues().split(",")))));
                }
                if (parametersManager.getAlgorithm().equals("i")) {
                    sorter = new InsertionSort();
                    System.out.printf("Ordenamiento: \n%s", sorter.sort(new ArrayList<>(Arrays.asList(parametersManager.getValues().split(",")))));
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}

