package Activity3.parameters;

import java.util.HashMap;
import java.util.Set;

public class ParametersValidator {
  private HashMap<String, String> parameters;

  public ParametersValidator(String[] arguments){
    validateVariables(arguments);
    validateVariableValues();
  }
  public HashMap<String, String> getParameters(){
    return parameters;
  }

  private boolean validateVariableValues(){
    boolean isValid = false;
    if (validateAlgorithmSelection(parameters.get("a"))){
        if (validateType(parameters.get("t"))){
          if (validateColour(parameters.get("o"))){
            isValid = true;
          }
        }
    }
    return isValid;
  }
  private boolean validateVariables(String[] arguments) {
    parameters = new HashMap<>();
    Set<String> keySet = parameters.keySet();
    for (String entry : arguments) {

      if (entry.length() <= 2){
        // System.out.printf("Detalle CLI: Variable [%s] sin valores\n \n", entry.charAt(0));
        if (entry.length() == 1) {
          if (entry.equals("v"))
            entry = "v=";
          entry = entry + "= ";
        }
        entry = entry + " ";
      }
      String[] arr = entry.split("=");
      if(arr[0].equals("a") || arr[0].equals("t") || arr[0].equals("o") || arr[0].equals("v") ) {
        if (arr[1].equals(" ")) {
          arr[1] = "";
        }

        arr[1] = arr[1].replace("“", "");
        arr[1] = arr[1].replace("”", "");
        parameters.put(arr[0], arr[1]);
      }
      else System.out.printf("La variable [%s] no es valida para el programa\n", arr[0]);

    }
    if (!keySet.contains("v")) {
      // System.out.println("[v] no especificada, generando valores para el programa");
    if (parameters.get("t").equals("c"))
        parameters.put("v", "m,j,k,l,e,n,c,d,b,g,h,i,f,o,p,a");
    if (parameters.get("t").equals("n"))
        parameters.put("v", "5,8,15,16,1,2,9,10,11,6,7,13,14,3,4,12");
    else parameters.put("v", "");
    }
    if (!keySet.contains("a"))
      parameters.put("a", "i");
    if (!keySet.contains("o"))
      parameters.put("o", "w");

    boolean result = true;

    if (!keySet.contains("a")){
      System.out.println("La variable [a] no fue ingresada");
      result = false;
    }
    if (!keySet.contains("t")){
      System.out.println("La variable [t] no fue ingresada");
      result = false;
    }
    if (!keySet.contains("o")) {
      System.out.println("La variable [o] no fue ingresada");
      result = false;
    }
    return result ;
  }

  private boolean validateColour(String colour) {
    colour = colour.toLowerCase();
    boolean result = false;
    if ( colour.equals("b") || colour.equals("w"))
      result = true;
    return result;
  }

  private boolean validateType(String valuesType) {
    valuesType = valuesType.toLowerCase();

    if ( valuesType.equals("c") || valuesType.equals("n"))
      if (valuesType.equals("c") ){
        if (containsOnlyLetters(parameters.get("v")))
          return true;
      }
      else if (valuesType.equals("n")){
        if (containsOnlyNumbers(parameters.get("v")))
          return true;
      }
    return false;
  }

  private boolean validateAlgorithmSelection(String algorithmName) {
    boolean isValid = false;
    if (algorithmName.toLowerCase().equals("i")  ||
        algorithmName.toLowerCase().equals("b")  ||
        algorithmName.toLowerCase().equals("q") ){
      isValid = true;
    }
    return isValid;
  }

  private boolean containsOnlyLetters(String input) {
    input = input.replace(",", "");
    if (input.length() <= 0)
      return false;
    for (char c : input.toCharArray()) {
      if (!Character.isLetter(c)) {
        return false;
      }
    }
    return true;
  }

  private boolean containsOnlyNumbers(String input) {
    input = input.replace(",", "");
    if (input.length() <= 0)
      return false;
    for (char c : input.toCharArray()) {
      if (!Character.isDigit(c)) {
        return false;
      }
    }
    return true;
  }
}
